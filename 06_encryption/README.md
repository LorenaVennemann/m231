# Verschlüsselung <!-- omit in toc -->

# Inhaltsverzeichnis <!-- omit in toc -->
- [0. Einführungs-Videos](#0-einführungs-videos)
- [1. Caesar](#1-caesar)
  - [1.1. Aufgabenstellung](#11-aufgabenstellung)
    - [1.1.1. Teil 1 - Verschlüsseln](#111-teil-1---verschlüsseln)
    - [1.1.2. Teil 2 - Entschlüsseln](#112-teil-2---entschlüsseln)
    - [1.1.3. Ausprobieren und ein wenig herumspielen](#113-ausprobieren-und-ein-wenig-herumspielen)
- [2. PGP - Pretty Good Privacy](#2-pgp---pretty-good-privacy)
  - [2.1. Aufgabenstellung](#21-aufgabenstellung)
  - [2.2. Hinweise](#22-hinweise)
  - [2.3. Videoanleitung - Kleopatra - Windows - PGP Schlüsselpaar erstellen und Public-key exportieren](#23-videoanleitung---kleopatra---windows---pgp-schlüsselpaar-erstellen-und-public-key-exportieren)
  - [2.4. Videoanleitung - Kleopatra - Windows - Public-Key einer dritten Person importieren](#24-videoanleitung---kleopatra---windows---public-key-einer-dritten-person-importieren)
  - [2.5. Videoanleitung - Kleopatra - Windows - Nachricht verschlüsseln und signieren](#25-videoanleitung---kleopatra---windows---nachricht-verschlüsseln-und-signieren)
  - [2.6. Videoanleitung - Kleopatra - Windows - Nachricht entschlüsseln und verifizieren](#26-videoanleitung---kleopatra---windows---nachricht-entschlüsseln-und-verifizieren)
  - [2.7. Videoanleitung - Kleopatra - Windows - Fingerprint verifizieren](#27-videoanleitung---kleopatra---windows---fingerprint-verifizieren)
- [3. Hash(funktion)](#3-hashfunktion)
  - [3.1. Aufgabe - Überprüfung heruntergeladenen *installation images (ISO)*](#31-aufgabe---überprüfung-heruntergeladenen-installation-images-iso)
    - [3.1.1. Videoanleitung - Hashwert einer Datei überprüfen](#311-videoanleitung---hashwert-einer-datei-überprüfen)
  - [3.2. Aufgabe - Gleicher Input gleicher Hash](#32-aufgabe---gleicher-input-gleicher-hash)
- [4. Veracrypt](#4-veracrypt)
  - [4.1. Aufgabenstellung](#41-aufgabenstellung)
  - [4.2. Hinweise](#42-hinweise)
  - [4.3. Videoanleitung - Veracrypt](#43-videoanleitung---veracrypt)
- [5. Cryptomator](#5-cryptomator)
  - [5.1. Aufgabenstellung](#51-aufgabenstellung)

# 0. Einführungs-Videos

Bei der symmetrischen Verschlüsselung wird derselbe Schlüssel zum verschlüsseln und entschlüsseln der Nachricht (Daten) gebraucht. 
Im Video [Symmetrische Verschlüsselung](https://studyflix.de/informatik/symmetrische-verschlusselung-1610) wird dies noch genauer erklärt.

Bei der assymetrischen Verschlüsselung wird ein Schlüsselpaar gebraucht. Um eine Nachricht zu verschlüsseln braucht man den einen der beiden Schlüssel und man kann dann die Nachricht nur mit dem anderen Schlüssel des Paar entschlüsseln.Im Video [Assymetrische Verschlüsselung](https://studyflix.de/informatik/asymmetrische-verschlusselung-1609) wird dies noch genauer erklärt

# 1. Caesar
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Klassenchallenge |
| Aufgabenstellung  | Nachrichten verschlüsseln und Entschlüsseln, CrypTool kennenlernen |
| Zeitbudget  |  1 Lektion |
| Ziel | Sie lernen das CrypTool kennen. |

## 1.1. Aufgabenstellung

In ersten Schritt verschlüsseln Sie mithilfe der [Caesar-Verschlüsselung](https://de.wikipedia.org/wiki/Caesar-Verschl%C3%BCsselung) eine Nachricht und schicken diese dem Kollegen der nach Ihnen in der Klassenliste folgt. Im zweiten Schritt versuchen Sie die Nachricht, die Sie vom Klassenkameraden vor Ihnen in der Klassenliste steht, zu entschlüsseln. Dafür verwenden Sie zwei verschiedene Tools: 
 - [CrypTool-Online - Caesar](https://www.cryptool.org/en/cto/caesar)
 - [CrypTool 2](https://www.cryptool.org/en/ct2/)



### 1.1.1. Teil 1 - Verschlüsseln
 - Besuchen Sie die Webseite [CrypTool-Online - Caesar](https://www.cryptool.org/en/cto/caesar). Fügen Sie Ihre Nachricht im Feld *Input (plaintext)* ein und wählen einen zufälligen Key aus. Wenn Ihnen keine Nachricht einfällt, nehmen sie doch ein  [zufälliges Zitat](http://zitate.net/zuf%C3%A4llige-zitate).
 - Wählen Sie nun eine zufällige Zahl (Ihren geheimen Schlüssel) aus und stellen diesen bei Key ein. 
 - Kopieren Sie Wert im Feld *Output (ciphertext)* (Die verschlüsselte Nachricht) und schicken es dem Klassenkameraden der nach Ihnen in der Klassenliste steht. 

![CrypTool-Online - Caesar - Encryption](./videos/cryptoolonlinecaesar.webm)

### 1.1.2. Teil 2 - Entschlüsseln
Nun kommt der spannende Teil. Obwohl es bei der Caesar-Verschlüsselung bei der Verwendung des einfachen Alphabetes nur 26 Buchstaben und demnach nur 25 mögliche Schlüssel gibt und das Durchprobieren dieser Möglichkeiten nicht sehr viel Zeit in Anspruch nimmt. Kann mithilfe der Analyse der Buchstabenhäufigkeit die Caesar-Verschlüsselung einfach geknackt werden. Das Video unten zeigt wie mithilfe des [CrypTool 2](https://www.cryptool.org/en/ct2/downloads) die verschlüsselte Nachricht innerhalb von zwei Minuten "geknackt" bzw. entschlüsselt werden kann. Der verwendete Schlüssel muss dabei nicht bekannt sein. 

**Können Sie die Nachricht Ihres Kollegen mit dieser Methode entschlüsseln?**

![CrypTool2 - Ceasar-Verschlüsselung Knacken](./videos/cryptool2caesardecryption.webm)


### 1.1.3. Ausprobieren und ein wenig herumspielen
Mithilfe von CrypTool können Sie unzählig verschiedene klassische und moderne Verschlüsselungsmethoden ausprobieren. Probieren Sie zum Beispiel die [Vigenère-Verschlüsselung](https://de.wikipedia.org/wiki/Vigen%C3%A8re-Chiffre) aus. 

Im nachfolgenden Video sehen Sie, wie Sie mithilfe des Wizards die verschiedenen Verschlüsselungen und Analysemethoden ausprobieren können. 

![CrypTool2 Wizard Vigenère-Verschlüsselung](./videos/cryptool2vigenere.webm)

# 2. PGP - Pretty Good Privacy
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Klassenchallenge |
| Aufgabenstellung  | Nachrichten mit PGP verschlüsseln und Entschlüsseln. |
| Zeitbudget  |  1 Lektion |
| Ziel | Sie lernen das CrypTool kennen. |

Pretty Good Privacy ist ein Programm zur Verschlüsselung und zum Unterschreiben von Daten. Es wird die Ver- und Entschlüsselung von E-Mails, Dateien, Ordner und ganzen Dateipartionen verwendet. 

## 2.1. Aufgabenstellung
 - Installieren Sie https://www.gpg4win.org/ auf Ihrem PC. <br/><i> - Das PGP Plugin für Outlook führte in der Vergangenheit immer wieder zu Problemen. Falls Ihr Outlook nicht mehr ordnungsgemäss funktioniert, aktualisieren sie GPG4WIN oder deaktivieren Sie das Plugin. <br/> - Sie müssen nichts spenden. Wählen Sie einfach 0 USD aus.</i>
 - Nach der Installation öffnet sich Kleopatra. Erstellen Sie mit *New Key Pair* ihr persönliches Schlüsselpaar. Geben Sie dabei die E-Mail Adresse ein mit der Sie das Schlüsselpaar verwenden möchten. 
 - Die öffentlichen Keyserver von OpenPGP funktionieren seit einer Weile nicht mehr. Deshalb müssen Sie die Schlüssel manuell austauschen. Schicken Sie Ihrem öffentlich Key (ASC Datei) an einer Ihren Mitlernenden. 
 - Dieser schickt Ihnen nun eine Antwort. 

## 2.2. Hinweise
 - Den Public Key müssen sie jeweils nur einmal mitschicken.
 - Verifizieren Sie den Fingerprint von erhaltenen Public-Keys (z.B. über einen zweiten Kanal: Telefon, Signal, Threema)
 - Kleopatra ist nicht die einzige Anwendung für PGP. Sie dürfen auch eine andere verwenden.

## 2.3. Videoanleitung - Kleopatra - Windows - PGP Schlüsselpaar erstellen und Public-key exportieren
![PGP new keypair](videos/pgpnewkeypair.webm)

## 2.4. Videoanleitung - Kleopatra - Windows - Public-Key einer dritten Person importieren
![PGP import public key](videos/pgpimportpublickey.webm)

## 2.5. Videoanleitung - Kleopatra - Windows - Nachricht verschlüsseln und signieren
![PGP encrypt file](videos/pgpencrypt.webm)

## 2.6. Videoanleitung - Kleopatra - Windows - Nachricht entschlüsseln und verifizieren
![PGP decrypt file](videos/pgpdecrypt.webm)

## 2.7. Videoanleitung - Kleopatra - Windows - Fingerprint verifizieren
Um eine erfolgreiche Man-in-the-middle Attacke unwahrscheinlicher zu machen, empfiehlt es sich sehr den erhaltenen Fingerprint zu verifizieren. Dafür muss beim Importieren im Schritt *Certify* *Cancel* gewählt werden und anschliessend der Fingerprint manuell überprüft werden. Als Beispiel für eine "sichere" Übertragung: Der Public-Key schicken Sie per E-Mail und der Fingerprint schicken Sie per Threema (Chat-App). 

Fingerprint mithilfe von Powershell manuell überprüfen:<br/>
![PGP verify fingerprint](videos/pgpverifyfingerprint.webm)

Public Key im nachhinein manuell zertifizieren:<br/>
![PGP manually certify public key](videos/pgpcertifypublickey.webm)


# 3. Hash(funktion)
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Der Hashwert einer Datei berechnen.  |
| Zeitbudget  |  1 Lektion |
| Ziel | C1G: Kann das Prinzip der Verschlüsselung von Daten erläutern |

Eine [Hashfunktion](https://de.wikipedia.org/wiki/Hashfunktion) ist eine Abbildung, die eine grosse Eingabemenge auf eine kleinere Zielmenge (den Hashwert) abbildet. Für die Datensicherheit sind vor allem die [kryptographische Hashfunktionen](https://de.wikipedia.org/wiki/Kryptographische_Hashfunktion) relevant. Diese Funktionen nehmen eine Bit- oder Bytefolge als Input und erzeugen daraus einen Hash von fixer Länge. Dabei haben diese Funktionen zwei wichtige Eigenschaften:
 - Kollisionsresistent: Es ist praktisch nicht möglich, zwei unterschiedliche Eingabewerte zu finden, die einen identischen Hash ergeben.
 - Einwegfunktion: Es ist praktisch nicht möglich, aus dem Hash den Eingabewert zu rekonstruieren.

Je nach Anwendungsbereich gelten unterschiedliche Anforderungen an die Hashfunktion. Die dabei in der praxis eingesetzten Verfahren ändern sich fortlaufend. Aktuell gilt folgende Empfehlung: 

| Zweck | Hashfunktion  |
|---|---|
| Überprüfung der Datenintegrität einer Datei  | SHA-256 (Mindestens MD5, SHA-1)  |
| Einsatz in Verschlüsselung (Container, VPN, usw.)  | Mindestens SHA-256, besser SHA-512, Whirlpool    |

## 3.1. Aufgabe - Überprüfung heruntergeladenen *installation images (ISO)*
Ein häufiger Einsatzzweck von Hashfunktionen ist die Überprüfung von Checksummen heruntergeladenen Dateien. Dabei wird sichergestellt, dass es sich bei der heruntergeladenen Datei zu keinen Fehlern bei der Übermittlung gekommen ist. 

Mithilfe von Powershell lässt sich dies sehr einfach überprüfen.

Der nachfolgende Befehl gibt für eine *DATEI* und einen *HASH* *True* aus, wenn der berechnete Hashwert der Datei mit dem Mitgebenden übereinstimmt oder *False*, falls das nicht der Fall ist. 

```powershell
(Get-FileHash DATEI -Algorithm SHA512).Hash -eq "HASH"
```

Beispiel für ```debian-11.2.0-amd64-netinst.iso```
```powershell
(Get-FileHash debian-11.2.0-amd64-netinst.iso -Algorithm SHA512).Hash -eq "c685b85cf9f248633ba3cd2b9f9e781fa03225587e0c332aef2063f6877a1f0622f56d44cf0690087b0ca36883147ecb5593e3da6f965968402cdbdf12f6dd74"
```

### 3.1.1. Videoanleitung - Hashwert einer Datei überprüfen
[Video - Cryptool - Hash](videos/hashoffile.webm)

## 3.2. Aufgabe - Gleicher Input gleicher Hash
Um ein besseres Gefühl für kryptografische Hashfunktionen zu erhalten, empfiehlt es sich verschiedene Hashfunktionen mit dem Cryptool auszuprobieren. Schauen Sie sich das nachfolgende Video an und versuchen Sie dasselbe mit anderen Hashwerten. Welche Hashwerte können Sie googlen, welche nicht?

[Video - Cryptool - Hash](videos/cryptoolhash.webm)


# 4. Veracrypt
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Mit Veracrypt ein verschlüsselter Container erstellen.  |
| Zeitbudget  |  1 Lektion |
| Ziel | C1F: Richtet und setzt Verschlüsselungen für Daten ein. |

"VeraCrypt ist eine Software zur Datenverschlüsselung, insbesondere zur vollständigen oder partiellen Verschlüsselung von Festplatten und Wechseldatenträgern." ([Wikipedia - Veracrypt](https://de.wikipedia.org/wiki/VeraCrypt))

Verschlüsselung soll sicherstellen, dass nur autorisierte Empfänger auf vertrauliche Daten, Informationen und Kommunikation zugreifen können. Üblicherweise wird Verschlüsselung für folgende Zwecke eingesetzt:
 - Kontinuierliche Verschlüsselung von Datenströmen (z.B. HTTPS bzw. SSL/TLS)
 - Verschlüsselung von Laufwerken und Container (z.B. Veracrypt, LUKS, Bitlocker)
 - Verschlüsselung von einzelnen Dateien oder Nachrichten (z.B. PGP, 7z (AES))

[Veracrypt](veracrypt.fr) fällt in den Verwendungszweck "Verschlüsselung von Laufwerken und Container". Im Vergleich zu der Verschlüsselung von einzelnen Dateien hat dieser Verwendungszweck den Vorteil, dass alle Dateien im Laufwerk automatisch verschlüsselt sind und nicht jede Datei durch aktive Betätigung des Benutzers einzeln verschlüsselt werden muss.

## 4.1. Aufgabenstellung
Mit Veracrypt können Sie ganze Datenträger verschlüsseln. In dieser Übung verwenden wir jedoch nur die Funktion der Erstellung eines Containers. 

**Ziele der Aufgabe**:
 - Veracrypt auf dem eigenen Computer installieren.
 - Einen neuen mit Veracrypt erstellen.
 - Den Container mounten. 
 - Den neuen Container mit Dateien befüllen. 
 - Den Container schliessen und nochmals mounten.

## 4.2. Hinweise
 - Ein Veracrypt-Container hat eine fixe Grösse. Das bedeutet, dass ein Container mit 5GB eine genauso grosse (.hc) Datei erstellt. Dieser Umstand setzt deshalb voraus, dass vor der Erstellung des Containers abgeschätzt werden muss, wie gross der Container sein muss, sodass alle zu verschlüsselnde Dateien darin Platz haben und das danach ein wenig Platz übrig bleibt. Letzteres ist notwendig, damit die Grösse des Containers nicht ständig angepasst werden muss. Bei der Wahl der Container-Grösse gilt der Grundsatz: *So klein wie möglich, aber so gross wie nötig.* 

## 4.3. Videoanleitung - Veracrypt
In diesem Video werden alle Schritte gezeigt, die für das Erreichen der Aufgabenziele benötigt werden. 
![Videoanleitung - Veracrypt](videos/veracrypt.webm)

# 5. Cryptomator
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Verschlüsselter Container und Filehosting-Dienste einrichten  |
| Zeitbudget  |  1 Lektion |
| Ziel | C1F: Richtet und setzt Verschlüsselungen für Daten ein. |

Dank DropBox, Google Drive, OneDrive, iCloud lassen sich Dateien im Handumdrehen über mehrere Geräte synchronisieren. Die meisten Filehosting-Dienste kosten für die ersten paar Gigabytes nichts und sind gut in allen gängigen Betriebssystemen implementiert. Das Problem: Die eigenen Dateien werden unverschlüsselt auf den Server des Providers gespeichert. Und selbst wenn der Provider die Dateien verschlüsselt, besitzt dieser die Schlüssel. 

Damit man von diesem Angeboten profitieren und dabei trotzdem seine Privatsphäre schützten kann, gibt es praktische Tools, wie der [Crytomator](https://cryptomator.org/), der es erlaubt verschlüsselte Container auf Filehosting-Dienste einzurichten. Im Vergleich zu Veracrypt hat der Container keine fixe Grösse und splitet die Verschlüsselten Daten in mehrere einzelne Files auf, sodass diese einfacher synchronisiert werden können. 

## 5.1. Aufgabenstellung
Sofern Sie einen Filehosting-Dienste verwenden, laden Sie sich Cryptomator herunter und richten Sie sich ein verschlüsseltes Volumen ein. 

[Auf Youtube finden Sie dazu zahlreiche Videos.](https://www.youtube.com/results?search_query=cryptomator)
