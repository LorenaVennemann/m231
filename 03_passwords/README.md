# Authentifizierung mit Passwörter <!-- omit in toc -->

# Inhaltsverzeichnis <!-- omit in toc -->
- [1. Bezug zur Modulidentifikation](#1-bezug-zur-modulidentifikation)
  - [1.1. Handlungsziele](#11-handlungsziele)
  - [1.2. Handlungsnotwendige Kenntnisse](#12-handlungsnotwendige-kenntnisse)
  - [1.3. Kompetenzen](#13-kompetenzen)
- [2. Einleitung](#2-einleitung)
- [3. Assoziationsübung Thema Passwörter](#3-assoziationsübung-thema-passwörter)
- [4. Notwendigkeit von Passwörter](#4-notwendigkeit-von-passwörter)
- [5. Authentifizierung und Autorisierung](#5-authentifizierung-und-autorisierung)
- [6. Regeln für sichere Passwörter](#6-regeln-für-sichere-passwörter)
  - [6.1. Einleitung](#61-einleitung)
  - [6.2. Voraussetzungen](#62-voraussetzungen)
  - [6.3. Ziel](#63-ziel)
  - [6.4. Vorgehen](#64-vorgehen)
  - [6.5. Tipps](#65-tipps)
- [7. Have I been pwned?](#7-have-i-been-pwned)
  - [7.1. Quellen](#71-quellen)
  - [7.2. Fragen](#72-fragen)
- [8. Regeln für sichere Passwörter](#8-regeln-für-sichere-passwörter)
  - [8.1. Quellen](#81-quellen)
- [9. Prinzip Three-factor Authentifizierung](#9-prinzip-three-factor-authentifizierung)
- [10. Der eigene Passwort-Manager](#10-der-eigene-passwort-manager)
  - [10.1. Austausch](#101-austausch)
  - [10.2. Vergleich](#102-vergleich)
  - [10.3. Eigener Passwortmanager auswählen](#103-eigener-passwortmanager-auswählen)
    - [Beispiel](#beispiel)
  - [10.4. Passwortmanager einrichten](#104-passwortmanager-einrichten)
- [11. Fallbeispiel](#11-fallbeispiel)
- [12. Quellen](#12-quellen)

# 1. Bezug zur Modulidentifikation
## 1.1. Handlungsziele
 - **2.** Überprüft und verbessert gegebenenfalls die Datensicherheit der eigenen Infra-struktur.

## 1.2. Handlungsnotwendige Kenntnisse
 - **2.3.** Kennt Techniken des Zugriffsschutzes, Passwortmanager und Prinzipien der Passwortverwaltung.
 - **2.4** Kennt den Unterschied von Authentifizierung und Autorisierung.

## 1.3. Kompetenzen
 - **E1G:** Kann die Prinzipen der Passwortverwaltung und "gute" Passwörter erläutern.
 - **E1F:** Kann einen Passwort Manager einsetzen.
 - **E1E:** Kennt und setzt erweiterte Möglichkeiten der Authentifizierung ein. Kennt Vor- und Nachteile von Passwortmanagern.

# 2. Einleitung

| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Leseauftrag |
| Zeitbudget  |  1/2 Lektion |
| Ziel | Einarbeitung in das Thema |

Die Identifizierung mit Benutzername und Passwort ist die am häufigsten verwendete Art, um Zugang zu einer geschützten Ressource zu erhalten. Während der Benutzername meist eine öffentlich bekannte E-Mail Adresse ist oder vom Namen der Person abgeleitet wird, ist das Passwort eine zufällig ausgewählte Folge von Zeichen (theoretisch). Schaut man auf dem GitHub Repository [SecList](https://github.com/danielmiessler/SecLists/blob/master/Passwords/xato-net-10-million-passwords-10.txt), welche aus verschiedenen Quellen die am häufigsten verwendeten Passwörter zusammengestellt hat, sieht man, dass "123456", "password" oder "dragon" zu den häufigsten Passwörtern gehören. Daraus lässt sich schliessen, dass die User ihre Passwörter nicht rein zufällig wählen, sondern die Tendenz haben, einfache Kombinationen zu wählen.

Um nachzuvollziehen, weshalb es Passwörter überhaupt braucht, muss man den Begriff der Identifizierung zuerst verstehen. Der "Projektbericht Sichere Digitale Identitäten", der vom DIN (Deutschen Institut für Normung) und der DKE (Deutschen Kommission Elektrotechnik) herausgegeben wurde, beschreibt diesen Begriff ganz allgemein (S.87).

<blockquote>
Ausgangspunkt einer digitalen Identität ist zunächst eine Entität (Kap. 3.2.1), also eine existierende konkrete oder abstrakte Sache, für die eine digitale Identität geschaffen bzw. erzeugt wird. Dies geschieht dadurch,  dass  zunächst  gewisse  Attribute  der  Entität  festgestellt  (Kap.  3.2.2)  ggf.  festgelegt  und  in  einem  digitalen  Datensatz  abgebildet  (Kap.  3.2.3)  werden.  Dann  wird  die  digitale  Identität  eingerichtet,  also  die  Entität  mit  der  digitalen  Identität  (dem  Datensatz  mit  der  Sammlung  von  Attributen)  verbunden  (Kap 3.2.4). Dies geschieht bspw. durch ein eineindeutiges Merkmal, welches die Entität besitzt oder welches auf sie aufgebracht oder integriert wird (Kap. 3.2.4.1). „Eineindeutig“ bedeutet dabei, dass ein Attributsatz nur für diese eine Entität existiert -  also einmalig ist. Nun wird der digitale Datensatz gespeichert und u.a. beim „Nutzen“ der Entität gepflegt, aktualisiert bzw. verwaltet (Kap. 3.2.5). Wird die Entität in einem Prozess in irgendeiner  Weise  genutzt,  kann  durch  das  eineindeutige  Merkmal  die  digitale  Identität  aufgerufen  und  bspw.  zur  Verifikation  genutzt  werden  (Kap.  3.2.6).  Dies  geschieht  so  lange  bis  die  Entität  bspw.  unersetzt  vernichtet  wird  und  ein  Aufbewahren  der  digitalen  Identität  ggf.  nicht  mehr  notwendig  ist.  Je  nach  Anwendungsfall wird die digitale Identität dann gelöscht oder gesperrt und archiviert.

In  einem  einfachen  Beispiel  bedeutet  dies  (vgl.  Abbildung  21):  Eine  Person  packt  ein  Paket  und  möchte  dies  verschicken.  Das  Paket  ist  die  Entität  (Kap.  3.2.1)  für  die  in  der  Postfiliale  eine  Digitale  Identität  geschaffen  wird.  Der  Postangestellte  stellt  fest  (Kap.  3.2.2),  dass  das  Paket  20  ×  20  cm  groß  ist,  400  g  schwer und erfragt die Empfängerdaten. Er gibt die Daten in sein System ein (schafft also ein Abbild, Kap. 3.2.3)  und  erstellt  eine  eineindeutige  Paketnummer,  die  per  Strichcode-Aufkleber  auf  das  Paket  geklebt  (eingerichtet,  Kap.  3.2.4)  wird.  Die  digitale  Identität  wird  in  dem  weltweit  zugreifbaren  System  der  Post  gespeichert  (verwaltet,  Kap.  3.2.5)  und  jedes  Mal,  wenn  das  Paket  bei  Zwischenstationen  ankommt,  wird  der  Code  eingescannt  und  somit  festgestellt,  welches  Paket  es  ist  und  welche  Bestimmung  es  hat  (Kap. 3.2.6). Der Ort wird in der Digitalen Identität aktualisiert (Nutzen, Kap. 3.2.6 und Verwalten, Kap. 3.2.5) und wenn das Paket zugestellt wurde, kann diese Digitale Identität auch wieder gelöscht werden (Kap. 3.2.7). 
</blockquote>

Das obige Beispiel des Projektberichtes beschreibt, wie sich ein Paket mithilfe des Strichcodes gegenüber den Zwischenstationen (z.B. eine automatische Paket-Sortieranlage oder der Scan-Gerät des Postboten). Der Strichcode ist dabei sichtbar aufgeklebt und es gibt keine Vorkehrungen, um missbrauch zu verhindern. Käme ein Paket auf die Idee sich als ein anderes Paket auszugeben, weil es dessen Nummer im Transportlastwagen abgelesen hat, könnte es sich zu einem anderen Ort transportieren lassen. Vermutlich würde das System einen Alarm an das Monitoring machen, weil ein Paket plötzlich andere Masse hat oder weil diesselbe Paket-Nummer bereits erfasst wurde. 

<blockquote>
Einrichten der besonderen physischen Entität - Mensch 
Der Mensch ist eine besondere Form der Entität. Durch seine grundsätzlich vorhandenen unterschiedlichen Fähigkeiten ist er ein Sonderfall, auch wenn diese Fähigkeiten durchaus mit den Möglichkeiten bei Dingen vergleichbar sind. Der Mensch hat in jedem Fall eigene Sicherheitsmerkmale im Sinne  der  Biometrie  oder  des  typischen  Verhaltens.  Zudem  kann  er  Dinge  sicher  speichern  (Passwort,  Gestenmuster, etc.). Er kann Situationen erkennen und analysieren, kann aktiv kommunizieren. Der Mensch kann aber keine höheren kryptographischen Prozesse vornehmen oder größere Datenmengen so speichern, dass sie für die IT unmittelbar verwendbar sind. Er kann aber Träger von Datenspeichern, Secure Elements  und  Hochsicherheitsmodulen  verwahren.  In  diesem  Sinne  ist  der  Mensch  prädestiniert  für  die  Kombination  mit  weiteren  Authentifikationsmitteln.  Bspw.  können  RFID-Chip  oder  Hochsicherheitsmodul  in Form einer Chipkarte für die Verifikation der Entität Mensch genutzt werden (z.B. Bankkarten, elektronischer  Personalausweis).  Wenn  die  Daten  auf  der  Chipkarte  oder  die  digitale  Identität  dahinter  auch  noch  Informationen  über  die  Biometrie  der  Person  beinhalten,  kann  diese  zur  Verifikation  der  Verbindung  Mensch  und  Chipkarte  dienen.  Das  meistgenutzte  Verfahren  hierbei  ist,  dass  der  Mensch  ein  Sicherheitsmerkmal“integriert“  mit  dem  er  die  Verifikation  der  Verbindung  zwischen  Chipkarte  und  Mensch vornehmen kann: das Passwort.  Bei der Entität Mensch werden bspw. folgende die Authentifizierungsmöglichkeiten unterschieden: 
• per Wissen aufgeteilt in Erinnern (Passwort, Unterschrift, Gestenmuster o.ä.) oder Erkennen 
(Geheimnis auf Bildern erkennen o.ä.),  
• per Besitz (Chipkarte)  
• per Biometrie 
</blockquote>

Die grosse Herausforderung bei der "Entität Mensch" ist, dass ein Missbrauchspotential vorhanden ist. Konkret bedeutet das, dass eine Entität versucht, sich als eine andere Entität gegenüber einer dritten Stelle zu authentifizieren. Einfacher ausgedrückt: Eine unberechtigte Person versucht in ein fremdes System einzudringen. 

Weiter Erfahren wir aus dem Abschnitt "Einrichten der besonderen physischen Entität - Mensch", dass eine Unterscheidung zwischen den Authentifizierungsmöglichkeiten gemacht wird:
 - per Wissen aufgeteilt in Erinnern (Something you know)
 - per Besitz (Something you have)  
 - per Biometrie (Something you are)

Unter einer Mehrfaktor-Authentifizierung versteht man eine Authentifizierung, die unterschiedliche Authentifizierungsmöglichkeiten nutzt. Eine häufig verwendete Methode ist auch das Zuschicken eines Codes an das Mobiltelefon des Benutzers (mTAN), welches gerne von den Banken verwendet wurde, aber immer mehr durch andere Verfahren ersetzt wird. 

In diesem Teil des Moduls 231 werden Sie sich mit dem Thema der Passwörter beschäftigen und die Themen "Was ist ein sicheres Passwort" und "Passwort-Verwaltung" selbstständig erarbeiten. 

# 3. Assoziationsübung Thema Passwörter 
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Assoziationsübung |
| Zeitbudget  |  10 Minuten |
| Ziel | Vorwissen zum Thema "Passwörter" aktivieren.  |

Die Instruktionen zu dieser Aufgabe erhalten Sie von der Lehrperson. 


# 4. Notwendigkeit von Passwörter
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Gruppenarbeit, 3er Gruppen |
| Aufgabenstellung  | Austausch zum Thema Passwörter |
| Zeitbudget  |  20 Minuten |
| Ziel | Grundlagen für die Notwendigkeit von Passwörter verstehen. |

Erstellen Sie in 3er Gruppen auf einem A3 ein Mindmap zu den folgenden beiden zentralen Fragen (Die kursiv geschrieben Fragen dienen lediglich zum besseren Verständnis):

 - Warum Passwörter?<br><i>Grund für den Einsatz von Passwörter: Weshalb werden Passwörter eingesetzt?</i>
 - Was wird geschützt?<br><i>Was wird mit Passwörter geschützt?<br>Welche Arten von Informationen werden mit Passwörter geschützt, welche sind frei verfügbar?</i>

Fügen Sie dieses Mindmap in ihr Portfolio ein!

# 5. Authentifizierung und Autorisierung
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Leseauftrag, Beantworten von Verständnisfragen |
| Zeitbudget  |  15 Minuten |
| Ziel | Den Unterschied zwischen den Begriffen  Authentifizierung und Autorisierung verstehen. |

Zur Anmeldung an einem Computer oder Netzwerk müssen Sie Ihren Benutzernamen und Ihr Kennwort eingeben. Dieser Vorgang wird Authentifizierung genannt. Er ermöglicht es, einen
Netzwerkbenutzer (eindeutig, wenn jeder Benutzer über ein eigenes Konto verfügt) einer bestimmten Person zuzuordnen.

Nach erfolgreicher Authentifizierung wird in Form der Autorisierung überprüft, welche Zugriffsrechte ein Benutzer im Netzwerk hat bzw. ob dieser die Erlaubnis hat, Software selbstständig zu installieren. Oder hier nochmal ausführlicher: [Dr. Datenschutz](https://www.dr-datenschutz.de/authentisierung-authentifizierung-und-autorisierung/)

Ordnen Sie die folgenden Aussagen die Begriffe Authentifizierung und Autorisierung zu. Nutzen Sie dafür dieses [MS Forms Quiz](https://forms.office.com/r/gMjdrcM7Da).
 - "Peter hat das Recht auf den Ordner *Geschäftsprozesse* zuzugreifen."
 - "Martina sperrt ihren Bildschirm vor der Kaffeepause und muss ihr Passwort eingeben, wenn Sie zurückkommt."
 - "Standard User haben keinen Zugriff auf die Server-Management-Konsole."
 - "Standardmässig kann in Linux Dateisystemen für jede Datei oder Ordner ein Benutzer, eine Gruppe, sowie die Zugriffsberechtigungen Lesen, Schreiben und Ausführen definiert werden."
 - RADIUS Server
 - "Nur Lehrerpersonen können in MS Teams einer Gruppe Lernende hinzufügen oder entfernen."
 - "Permission denied."
 - "Für das Online-Banking benötige ich mein Benutzername, mein Passwort und das Foto-TAN App."

# 6. Regeln für sichere Passwörter
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Praktische Übung mit eigenen Notebook |
| Zeitbudget  |  3 Lektionen |
| Ziel | Herausforderungen von sicheren Passwörtern mithilfe einer praktischen Übung besser verstehen. |

## 6.1. Einleitung
Nicht selten verwenden User persönliche Informationen, wie Postleitzahlen, Autonummern, Name des Haustieres oder Name der/des Ehepartner*in als Passwort. Das ist nicht zwangsläufig unsicher, aber es wird insbesondere dann unsicher, wenn ausser diesen Informationen keine unspezifischen Elemente vorhanden sind. Bei Security Reviews von Firmen wird gerne geprüft, ob Mitarbeiter ihre Passwörter auf diese Weise gewählt haben. 

In dieser kleinen Übung werden Sie selbst einen solchen Review durchführen. Dafür stehen ihnen folgende Mittel und Tools zur Verfügung.
 - Fake-Profile der Firma und von den zwei Mitarbeiter
 - Fake-ERP der Strauss Fricke AG
 - Python Applikation

## 6.2. Voraussetzungen
Damit Sie diese Übung durchführen können, benötigen Sie folgende Tools auf Ihrem PC:
 - Git: https://git-scm.com/downloads
 - Python 3.* https://www.python.org/downloads/

## 6.3. Ziel
 - Finden Sie alle mögliche Logins heraus. Wenn Sie sich erfolgreich ins System eingeloggt haben, wird eine *secret sentence* angezeigt. Die Person oder das Team, dass als erstes diesen Satz in den Teams Chat postet, gewinnt. 

## 6.4. Vorgehen
 - Lesen Sie die gesamte Übung durch
 - Installieren Sie allenfalls fehlende Tools auf Ihrem Notebook
 - Gehen Sie auf https://github.com/alptbz/seleniumbruteforce und folgen Sie den Instruktionen
 - In den Instruktionen steht, dass Sie *words.txt* befüllen müssen. Das Python-Skript *password_generator.py* nimmt diese Wörter und generiert daraus alle möglichen Variationen.
```
Beispiel:
In words.txt fügen Sie zwei Worte ein:
eins
zwei

password_generator.py wird in dictionary.txt folgendes reinschreiben (Zusätzliche Gross- und KleinSchreibungsvarianten weggelassen):
eins
zwei
einseins
zweieins
einszwei
zweizwei
einseinseins
zweieinseins
einszweieins
zweizweieins
einseinszwei
zweieinszwei
einszweizwei
zweizweizwei
```
 - Studieren Sie die Fake-Profile. Welche Informationen könnten Sie für die Generierung des *password dictionaries* verwenden?
 - Schreiben Sie einzelnen Elemente in *words.txt*. Es könnte zum Beispiel so aussehen:
```
9056
schachen
1974
371372
```
 - Denken Sie daran, dass je mehr Informationen Sie einfügen, desto länger dauert das *Bruteforcing*. 
 - Die Benutzernamen werden nach dem Schema *[Erster Buchstabe Vorname].[Nachname]* gebildet (ohne Umlaute!)* Beispiel: Aus *Timo Müller* wird *t.mueller*

## 6.5. Tipps
Bei der Installation von Python müssen Sie unbedingt, *Add Python 3.9 to PATH* auswählen, damit Sie python von *git bash* aufrufen können. 
![Python Path](images/python_installation_tipp.PNG)

# 7. Have I been pwned?
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Praktische Übung, Vertiefungsfragen |
| Zeitbudget  |  15 Minuten |
| Ziel | Die Frage "Lassen sich meine Benutzerdaten in geleakten Datendiebstähle finden?" für sich persönlich beantworten.  |

Keine Infrastruktur ist sicher. Es spielt keine Rolle wie viele Sicherheitsmechanismen vorhanden sind. Sobald ein System mit dem Internet verbunden wird, ist es nur eine Frage der Zeit bis ein Angriff versucht wird und auch erfolgreich ist. Am Besten lässt sich das Beispiel von namhaften Internationalen Firmen zeigen, die in den letzten Jahren grosse Datendiebstähle verzeichnet haben. 

Verschiedene Organisationen bieten einen kostenlosen Service an, der es Ihnen erlaubt zu prüfen, ob Ihre Daten in einem dieser Datendiebstähle vorhanden sind. 

Prüfen Sie mithilfe von [';--have i been pwned?](https://haveibeenpwned.com/) ob Sie bereits Opfer eines Datendiebstahls geworden sind. 

## 7.1. Quellen
 - [wikipedia - List of data breaches](https://en.wikipedia.org/wiki/List_of_data_breaches)
 - [Adobe Breach Impacted At Least 38 Million Users](https://web.archive.org/web/20210817162148/https://krebsonsecurity.com/2013/10/adobe-breach-impacted-at-least-38-million-users/)

## 7.2. Fragen
 - Weshalb macht es Sinn unterschiedliche Passwörter zu verwenden?
 - Mit welchen Ihrer persönlichen Zugangsdaten lässt sich am meisten Schaden anrichten?

# 8. Regeln für sichere Passwörter
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Gruppenarbeit, 3 - 4 Personen |
| Aufgabenstellung  | Leseauftrag |
| Zeitbudget  |  1 Lektion |
| Ziel | Eigene Regeln für sichere Passwörter festlegen. |

Setzen Sie sich mit den nachfolgenden Quellen auseinander und legen Sie fünf Passwort-Richtlinien fest. Halten Sie diese Regeln in ihrem Portfolio fest. 

## 8.1. Quellen
 1. https://www.ncsc.gov.uk/blog-post/the-logic-behind-three-random-words
 2. https://arstechnica.com/information-technology/2019/06/microsoft-says-mandatory-password-changing-is-ancient-and-obsolete/
 3. https://blog.codinghorror.com/password-rules-are-bullshit/
 4. https://github.com/dumb-password-rules/dumb-password-rules
 5. https://pages.nist.gov/800-63-3/sp800-63b.html
 6. https://github.com/danielmiessler/SecLists/pull/155
 7. https://www.netsec.news/summary-of-the-nist-password-recommendations-for-2021/


# 9. Prinzip Three-factor Authentifizierung
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Leseauftrag |
| Zeitbudget  |  20 Minuten |
| Ziel | Mehrfaktor-Authentifizierung verstehen |

In der Einleitung haben sie sich mit den verschiedenen Möglichkeiten der Authentifizierung auseinandergesetzt.

Erarbeiten Sie sich ein Verständnis für Mehrfaktor-Authentifizierung mithilfe der nachfolgenden Beiträge:
 - https://en.wikipedia.org/wiki/Multi-factor_authentication
 - https://dis-blog.thalesgroup.com/security/2011/09/05/three-factor-authentication-something-you-know-something-you-have-something-you-are/

# 10. Der eigene Passwort-Manager
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Praktische Übung |
| Zeitbudget  |  4 Lektionen |
| Ziele | Eigene Passwortverwaltungslösung eingerichtet und dokumentiert. |

Auf der Webseite von KeepassXC wird in einer Frage geklärt, weshalb jeder ein Passwortmanager verwendet soll:
<blockquote>
Password reuse and simple, easy-to-guess passwords are the <b>biggest problems</b> when using online services. If one service gets compromised (either by guessing your password or by exploiting a security vulnerability in the service's infrastructure), an attacker may gain access to all of your other accounts. But using different passwords for all websites is difficult without a way of storing them somewhere safe. Especially with arbitrary password rules for various services, it becomes increasingly hard to use both strong and diverse passwords. KeePassXC stores your passwords for you in an <b>encrypted database file</b>, so you only need to remember one master password. Of course, the security of all your services depends on the strength of your master password now, but with a sufficiently strong password, the password database should be infeasible to crack. The database is encrypted with either the industry-standard <b>AES256</b> or the <b>Twofish</b> block cipher and the master password is strengthened by a configurable number of key transformations to <b>harden it against brute force attacks</b>. Additionally, you can use a key file filled with an arbitrary number of random bytes or a YubiKey to further enhance your master key.
</blockquote>
<i>Quelle: https://keepassxc.org/docs/#faq-keepass</i>

## 10.1. Austausch
**Wichtig: Schauen Sie den Link im nächsten Abschnitt NICHT an!**
Diskutieren Sie ca. 5 Minuten mit Ihren Mitlernenden über das Thema Passwortmanager. Welche Kriterien denken Sie, sind bei der Wahl des Passwortmanagers wichtig? Halten Sie drei Punkte in einer Notiz fest. 


## 10.2. Vergleich
Lesen Sie das [Merkblatt Passwortmanager](https://docs.datenschutz.ch/u/d/publikationen/formulare-merkblaetter/merkblatt_passwortmanager.pdf) des Datenschutzbeauftragten des Kantons Zürichs durch. 

In diesem Dokument werden die wichtigsten Vergleichskriterien für Passwort Manager. 

Was halten Sie nun über Ihre zuvor festgehaltenen Kriterien? Sind das tatsächlich die wichtigsten Kriterien oder hat die neue Information Ihnen neue Erkenntnisse gebracht?

## 10.3. Eigener Passwortmanager auswählen
Auch wenn Sie bereits einen Passwortmanager haben: Suchen Sie sich einen Passwortmanager aus und begründen Sie mithilfe von messbaren und sachlichen Kriterien Ihre Präferenz. 

### Beispiel
Definieren Sie ihre Anforderungen:

 - **Anforderung Verschlüsselungsalgorithmus:** Die Passwortdatenbank muss eine sicheren Verschlüsselungsalgorithmus (AES-256, ChaCha20 oder ähnlich).
 - **Anforderung OpenSource:** Der Quellcode des Passwortmanagers muss öffentlich einsehbar sein. 

Prüfen Sie ob der Passwortmanager Ihre Anforderungen erfüllt:

|                 | **Keepass**         | Wertung | **Lastpass** | Wertung       |
|-----------------|-------------------|---------|----------|---------------|
| Link            | [keepass.info](https://keepass.info/)   |  | [lastpass.com](https://www.lastpass.com/de)  | erfüllt       |
| Verschlüsselung | AES-256, ChaCha20 | ✓ erfüllt | AES-256  | ✓ erfüllt       |
| OpenSource      | Ja                | ✓erfüllt  | nein     | ╳ nicht erfüllt |

Definieren Sie für Ihre Entscheidung mindestens 4 Kriterien und vergleichen Sie mindestens 3 unterschiedliche Passwortmanager. 

## 10.4. Passwortmanager einrichten
Sie haben sich für einen Passwortmanager entschieden. Nun ist es Zeit diesen auf Ihrem Notebook (und eventuell weiteren Geräten) einzurichten. Machen Sie sich zudem folgende Überlegungen:

 - Was tun Sie, wenn Sie ihr Master-Passwort vergessen?
 - Wie erstellen Sie ein Backup von Ihrer Passwortdatenbank?<br>*Wenn Sie ein Cloud Dienst verwenden: Was tun Sie, wenn Sie keinen Zugriff auf Ihren Account mehr haben?*

# 11. Fallbeispiel
 - Folge von Identitätsdiebstahl

# 12. Quellen
 1. https://lock.cmpxchg8b.com/passmgrs.html
 2. https://anonymousplanet.org/guide.html#appendix-a2-guidelines-for-passwords-and-passphrases
 3. https://github.com/danielmiessler/SecLists
 4. Din https://www.din.de/resource/blob/306552/1e281ee0a725f5569469af8285ff0183/din-dke-projektbericht-data.pdf


